# Django Gitlab Autodeploy Test

This is my test project for testing auto deploy and several other CI tasks with Gitlab!

Everything you see here is licensed under the [MIT license](LICENSE.md), so feel free to copy it (though be aware that most of it is probably garbage).

## Folders

- ``app`` - contains the source code of the django application
- ``logs`` - contains logs (empty in repo, but will be filled when you run the app locally)
- ``docker`` - contains docker stuff
- ``docs`` - contains docs (if there are any)
- ``venv`` - python virtual environment
- ``README.md`` - this file 
- ``LICENSE`` - license information
- ``docker-compose.prod.yml`` - production docker-compose.yml file
- ``docker-compose.dev.yml`` - docker-compose for development
