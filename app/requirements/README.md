# Requirements

All requirements should go into this directory.

- [base.txt](base.txt) contains all basic requirements
- [dev.txt](dev.txt) contains all basic requirements plus development requirements (such as django debug toolbar)
- [test.txt](test.txt) contains all basis requirements plus testing requirements (such as coverage)
- [prod.txt](prod.txt) contains all basic requirements plus production requirements
