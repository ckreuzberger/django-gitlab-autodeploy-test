from django.db import models


class Visitor(models.Model):
    """
    Visitor Model that stores each visit
    """

    ip = models.GenericIPAddressField()

    user_agent = models.TextField(
        verbose_name="arbitrary long user agent string"
    )

    datetime = models.DateTimeField(auto_now_add=True)

    request_path = models.TextField(
        verbose_name="arbitrary long request path"
    )

