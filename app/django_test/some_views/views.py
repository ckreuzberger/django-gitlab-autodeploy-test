from django.shortcuts import render
from django.views import View

# Create your views here.
from django_test.some_views.models import Visitor


class VeryBasicView(View):
    """
    Very basic view that just prints the number of visits
    """
    template_name = 'some_views/very_basic_view.html'

    def get(self, request, *args, **kwargs):
        Visitor.objects.create(
            ip=request.META['REMOTE_ADDR'],
            user_agent=request.META['HTTP_USER_AGENT'],
            request_path=request.get_full_path()
        )

        context = {
            'number_of_visits': Visitor.objects.all().count(),
            'last_visits': Visitor.objects.filter(ip=request.META['REMOTE_ADDR']).order_by('-datetime')[:5]
        }

        return render(request, self.template_name, context)
