from django.conf.urls import url

from django_test.some_views.views import VeryBasicView


app_name = 'some_views'

urlpatterns = [
    url(r'^$', VeryBasicView.as_view(), name='very-basic-view'),
]
